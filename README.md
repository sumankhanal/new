[![Actions Status](https://github.com/sumankhanal/new/workflows/test/badge.svg)](https://github.com/sumankhanal/new/actions)

NAME
====

new - blah blah blah

SYNOPSIS
========

```raku
use new;
```

DESCRIPTION
===========

new is ...

AUTHOR
======

sumanstats <suman81765@gmail.com>

COPYRIGHT AND LICENSE
=====================

Copyright 2022 sumanstats

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

