
sub mean(@data) is export {
  my $number_of_elements=@data.elems;
  return sum(@data)/$number_of_elements
}