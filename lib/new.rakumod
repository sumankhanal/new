unit module new;
use Desc::Mean;
use Deviation::DeviationMean;



=begin pod

=head1 NAME

new - blah blah blah

=head1 SYNOPSIS

=begin code :lang<raku>

use new;

=end code

=head1 DESCRIPTION

new is ...

=head1 AUTHOR

sumanstats <suman81765@gmail.com>

=head1 COPYRIGHT AND LICENSE

Copyright 2022 sumanstats

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
