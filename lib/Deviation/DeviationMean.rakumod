use Desc::Mean;

sub deviation_from_mean(@a) is export {
    my $mean = mean(@a);
    return @a »-» $mean
}